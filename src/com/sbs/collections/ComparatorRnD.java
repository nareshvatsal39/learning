package com.sbs.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ComparatorRnD {

    public static void main(String[] args) {

        List<Employee> employees = new ArrayList<>();
        Employee e1 = new Employee();
        e1.setAge(12);
        e1.setFirstName("abc");
        e1.setLastName("defab");
        Employee e2 = new Employee();
        e2.setAge(123);
        e2.setFirstName("aca");
        e2.setLastName("defaa");

        Employee e3 = new Employee();
        e3.setAge(103);
        e3.setFirstName("aba");
        e3.setLastName("defaba");

        employees.add(e1);
        employees.add(e2);
        employees.add(e3);
        Collections.sort(employees, new NameComparator());
        Collections.sort(employees, new AgeComparator());
        for (Employee e : employees) {
            System.out.println(".." + e.getFirstName());
            System.out.println(".." + e.getLastName());
            System.out.println(".." + e.getAge());
        }

    }
}

class NameComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {

        return o1.getFirstName().compareTo(o2.getFirstName());
    }
}

class AgeComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {

        return o1.getAge() - o2.getAge();
    }
}

class AgeComparable implements Comparable<Employee> {

    @Override
    public int compareTo(Employee o) {
        // TODO Auto-generated method stub
        return 0;
    }
}