package com.sbs.collections;

import java.util.HashSet;
import java.util.Set;

public class HashSetRndD {

    public static void main(String[] args) {

        Set<A> set = new HashSet<>();
        A a1 = new A();
        a1.setNumber(1);
        A a2 = new A();
        a2.setNumber(2);
        A a3 = new A();
        a3.setNumber(1);
        set.add(a1);
        set.add(a2);
        set.add(a3);
        System.out.println(set);

    }

}

class A {

    private int number;

    /**
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "A [number=" + number + "]";
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + number;
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        A other = (A) obj;
        if (number != other.number) {
            return false;
        }
        return true;
    }

}