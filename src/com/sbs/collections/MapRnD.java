package com.sbs.collections;

import java.util.HashMap;
import java.util.Map;

import com.sbs.model.Employee;

public class MapRnD {

    public static void main(String[] args) {
        Map<Employee, String> employeeMap = new HashMap<>();
        Employee e1 = new Employee(12, "abc");
        Employee e2 = new Employee(12, "abc");
        employeeMap.put(e1, "dept1");
        employeeMap.put(e2, "dept1");
        System.out.println("e1 hascode--" + e1.hashCode());
        System.out.println("e2 hascode--" + e2.hashCode());
        System.out.println(employeeMap);
    }

}
