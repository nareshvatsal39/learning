package com.sbs.java8.lambda;

public interface Sayable {

    String sayHello(String name);
}
