package com.sbs.java8.lambda;

public interface Addable {

    int add(int a, int b);
}
