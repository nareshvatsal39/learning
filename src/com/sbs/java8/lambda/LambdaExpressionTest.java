package com.sbs.java8.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class LambdaExpressionTest {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Addable ad1 = (a, b) -> (a + b);
        // System.out.println("addition for multiple parameteer----" + ad1.add(1, 2));
        Sayable s = name -> {
            return "welcome..." + name;
        };
        // System.out.println(s.sayHello("sai"));
        List<String> list = new ArrayList<String>();
        list.add("sai");
        list.add("mahesh");
        list.add("inder");
        list.add("jaikishan");
        // list.forEach(n -> System.out.println("name after iteration.." + n));
        Runnable r = () -> {
            System.out.println("thread is running");
        };
        Thread t = new Thread(r);
        // t.start();
        List<Product> products = new ArrayList<Product>();

        // Adding Products
        // products.add(new Product(1, "HP Laptop", 25000f));
        // products.add(new Product(3, "Keyboard", 300f));
        // products.add(new Product(2, "Dell Mouse", 150f));
        products.add(new Product(1, "Samsung A5", 17000f));
        products.add(new Product(3, "Iphone 6S", 65000f));
        products.add(new Product(2, "Sony Xperia", 25000f));
        products.add(new Product(4, "Nokia Lumia", 15000f));
        products.add(new Product(5, "Redmi4 ", 26000f));
        products.add(new Product(6, "Lenevo Vibe", 19000f));
        System.out.println("Sorting on the basis of name...");
        java.util.Collections.sort(products, (p1, p2) -> {
            return p1.name.compareTo(p2.name);
        });
        // products.forEach(product -> System.out.println(product.id + " " + product.name + " " +
        // product.price));

        Stream<Product> filtered_Data = products.stream().filter(product -> product.price > 20000);
        // filtered_Data.forEach(product -> System.out.println(product.name + ": " + product.price));

        JTextField tf = new JTextField();
        tf.setBounds(50, 50, 150, 20);
        JButton b = new JButton("click");
        b.setBounds(80, 100, 70, 30);
        b.addActionListener(e -> {
            tf.setText("hello swing");
        });

        JFrame f = new JFrame();
        f.add(tf);
        f.add(b);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLayout(null);
        f.setSize(300, 200);
        f.setVisible(true);
    }

}

class Product {
    int id;

    String name;

    float price;

    public Product(int id, String name, float price) {
        super();
        this.id = id;
        this.name = name;
        this.price = price;
    }
}