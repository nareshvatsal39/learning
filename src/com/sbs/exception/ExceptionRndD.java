package com.sbs.exception;

public class ExceptionRndD {

    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
        try{
            throw new TryException();
        }
        catch(Exception e){
            throw new CatchException();
        }finally{
            throw new FinallyException();
        }

    }

}

class TryException extends Exception {
}

class CatchException extends Exception {
}

class FinallyException extends Exception {
}
