package com.sbs.exception;

public class ExceptionSupreesion {

    String str = "a";

    void A() {
        try {
            str += "b";
            B();
        } catch (Exception e) {
            str += "c";
        } finally {
            System.out.println("finally called");
        }
    }

    void B() throws Exception {
        try {
            str += "d";
            C();
        } catch (Exception e) {
            throw new Exception();
        } finally {
            str += "e";
        }

        str += "f";

    }

    void C() throws Exception {
        throw new Exception();
    }

    void display() {
        System.out.println(str);
    }

    public static void main(String[] args) {
        ExceptionSupreesion object = new ExceptionSupreesion();
        object.A();
        object.display();
    }

}
