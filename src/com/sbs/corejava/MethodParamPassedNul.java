package com.sbs.corejava;

public class MethodParamPassedNul {

    public static void main(String[] args) {
        execute(null);
    }

    public static void execute(Test1 obj) {
        obj.print();
    }

}

class Test1 {
    public static void print() {
        System.out.println("hello");
    }
}