package com.sbs.corejava;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeParseException;
import java.util.Date;

public class DateFormatter {

    public static void main(String[] args) {

        // LOGGER.debug("Parsing date to Instant format, in DateValidator.validateFormat method.");
        try {
            // final Instant instant = java.time.Instant.parse(date);

            SimpleDateFormat entitlementSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            String entitlementDate = "2017-04-03T15:18:32.146+0000";
            Date entDate = entitlementSdf.parse(entitlementDate);

            // final Instant entitlementDate =
            // Instant.from(DateTimeFormatter.ISO_ZONED_DATE_TIME.parse("2017-04-03T15:18:32.146+0000"));
            // System.out.println("instant date" + instant);
            System.out.println("entitelement date " + entDate);
            // LOGGER.debug("Date: " + date + " parsed to Instant time: " + instant);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            String xiLodgingDate = "2018-08-17T00:00:00";
            Date date = sdf.parse(xiLodgingDate);
            System.out.println("xi lodging date " + date); // Tue Aug 31 10:20:56 SGT 1982
            if (date.equals(entDate)) {
                System.out.println("true");
            }

        } catch (final DateTimeParseException dateTimeParseException) {
            // LOGGER.error(APIErrorVO.CANNOT_PARSE_DATE.format(new String[] { date }),
            // dateTimeParseException);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
