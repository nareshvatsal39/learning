package com.sbs.corejava;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ListFileFromDirectory {

    public static void main(String[] args) {

        File folder = new File("c:\\carnival\\files");
        File[] listOfFiles = folder.listFiles();

        for (File listOfFile : listOfFiles) {
            if (listOfFile.isFile()) {
                System.out.println("File --" + listOfFile.getName());

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
                String nowAsISO = df.format(new Date(listOfFile.lastModified()));
                System.out.println("iso date ---" + nowAsISO);
            } else if (listOfFile.isDirectory()) {
                System.out.println("Directory --" + listOfFile.getName());
            }
        }
    }

}
