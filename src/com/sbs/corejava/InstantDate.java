package com.sbs.corejava;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class InstantDate {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG).withLocale(Locale.UK).withZone(ZoneId.systemDefault());
        Instant instant = Instant.now();
        String output = formatter.format(instant);
        System.out.println("--" + instant.toString());

    }

}
