package com.sbs.corejava;

import java.math.BigDecimal;

public class BigdecimalDivide {

    public static void main(String[] args) {
        BigDecimal total = new BigDecimal(10000);
        BigDecimal completed = new BigDecimal(3);
        BigDecimal progress = completed.divide(total, 3, BigDecimal.ROUND_UP);
        // System.out.println("progress" + progress);
        double doubleDivision = (double) 1 / (double) 1000;
        System.out.println("doubleDivision----" + doubleDivision);

    }

}
