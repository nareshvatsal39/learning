package com.sbs.corejava;

public class PassByRefValueRnD {

    public static void main(String[] args) {
        Bar bar = new Bar("bar");
        method1(bar); // here we are changing the instance that the reference variable "bar" refers
        System.out.println(" method1(bar)  " + bar.getProperty());
        method2(bar); // here we are not changing the reference!
        System.out.println(" method2(bar)  " + bar.getProperty());
    }

    public static void method1(Bar m1bar) {
        m1bar.setProperty("modifybar");

    }

    public static void method2(Bar m2bar) {
        Bar newbar = new Bar("newbar");
        m2bar = newbar;
    }

}

class Bar {
    private String property;

    public Bar(String property) {
        this.property = property;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}