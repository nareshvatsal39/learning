package com.sbs.corejava;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateComparsion {

    static String dateStr1 = "2016-11-16 06:43:19.769";

    static String dateStr2 = "2016-11-15T22:00:00";

    static DateFormat df = new SimpleDateFormat("yyyy-mm-dd");

    public static void main(String[] args) {

        Date date1;
        Date date2;
        try {
            date1 = df.parse(dateStr1);
            System.out.println("date1--" + date1);

            date2 = df.parse(dateStr2);
            System.out.println("date2---" + date2);
            if (date2.before(date1)) {
                System.out.println("Date2 is before Date1 ...");
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
