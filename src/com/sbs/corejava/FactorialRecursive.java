package com.sbs.corejava;

public class FactorialRecursive {

    public static void main(String[] args) {
        // System.out.println("---" + new FactorialRecursive().fact(8));
        int i, fact = 1;

    }

    int factRecursive(int n) {
        int i = 0;
        int result;
        if (n == 0 || n == 1) {
            return 1;
        }

        result = factRecursive(n - 1) * n;
        System.out.println("resut " + result);
        System.out.println("i value is " + i);
        return result;
    }

    void factNormal() {
        int number = 8;// It is the number to calculate factorial
        int fact = 0;
        for (int i = 1; i <= number; i++) {
            fact = fact * i;
        }
        System.out.println("Factorial of " + number + " is: " + fact);
    }

}
