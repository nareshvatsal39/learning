package com.sbs.corejava;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;

/**
 * Class will validate date time related details.
 *
 */
public class DateValidatorRnD {

    public static void main(String[] args) {

        validateInstantFormat("2016-10-21T00:00:00Z");
        validateInstantFormat("2016-10-21T00:00:00.5-04:00");
        validateInstantFormat("1997-07-16T19:20:30.45+01:00");

    }

    /**
     * Method will validate parsing of incoming date to Instant format.
     *
     * @param date
     * @return
     */
    public static boolean validateInstantFormat(String date) {
        // LOGGER.debug("Parsing date to Instant format, in DateValidator.validateFormat method.");
        try {
            // final Instant instant = java.time.Instant.parse(date);
            final Instant instant = Instant.from(DateTimeFormatter.ISO_ZONED_DATE_TIME.parse(date));
            // System.out.println("instant date" + instant);
            System.out.println("instant date" + instant);
            // LOGGER.debug("Date: " + date + " parsed to Instant time: " + instant);
            return true;
        } catch (final DateTimeParseException dateTimeParseException) {
            // LOGGER.error(APIErrorVO.CANNOT_PARSE_DATE.format(new String[] { date }),
            // dateTimeParseException);
            return false;
        }
    }

    /**
     * Method will validate date is less than week.
     *
     * @param date
     * @return
     */
    public static boolean isDateLessThanWeek(String date) {
        try {
            final Instant givenDate = java.time.Instant.parse(date);

            // Calculate date before 1 week from today.
            final Instant pastWeekDate = Instant.now().minus(7, ChronoUnit.DAYS);
            return givenDate.isAfter(pastWeekDate);
        } catch (final DateTimeParseException dateTimeParseException) {
            // LOGGER.info(APIErrorVO.CANNOT_PARSE_DATE.format(new String[] { date }),
            // dateTimeParseException);
            return false;
        }

    }

    /**
     * Method will validate time is not more than future 30 minutes.
     *
     * @param date
     * @return
     */
    public static boolean isTimeLessThan30Mins(String date) {
        try {
            final Instant givenDate = java.time.Instant.parse(date);
            // Calculate time with 30 mins ahead.
            final Instant currentDatePlus30Min = Instant.now().plus(30, ChronoUnit.MINUTES);
            return givenDate.isBefore(currentDatePlus30Min);
        } catch (final DateTimeParseException dateTimeParseException) {
            // LOGGER.info(APIErrorVO.CANNOT_PARSE_DATE.format(new String[] { date }),
            // dateTimeParseException);
            return false;
        }
    }
}
