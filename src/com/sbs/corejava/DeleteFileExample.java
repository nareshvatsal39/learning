package com.sbs.corejava;

import java.io.File;

public class DeleteFileExample {
    public static void main(String[] args) {
        try {

            String filePath = "c:\\carnival\\file\\";
            File file = new File(filePath.concat("test").concat(".txt"));

            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}