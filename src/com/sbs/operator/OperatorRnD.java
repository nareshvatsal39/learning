package com.sbs.operator;

public class OperatorRnD {

    public static void main(String[] args) {

        int a = 20;
        a = a++ + ++a + 9;
        int b = 0;
        int c = b++ + 2;
        int d = b + 10;
        System.out.println("c.." + c);
        System.out.println("b.." + b);
        System.out.println("d.." + d);

        // System.out.println(a);
        int i = 3;
        i++;
        System.out.println(i); // "4"
        ++i;
        System.out.println(i); // "5"
        System.out.println(++i); // "6"
        System.out.println(i++); // "6"
        System.out.println(i); // "7"

    }
}
