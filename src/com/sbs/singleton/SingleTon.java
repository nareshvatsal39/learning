package com.sbs.singleton;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SingleTon implements Serializable {

    /**
    *
    */
    private static final long serialVersionUID = 1L;

    private static SingleTon singleTonObj = null;

    private SingleTon() {
        System.out.println("creating object");
    }

    public static SingleTon getInstance() {

        if (singleTonObj == null) {
            synchronized (SingleTon.class) {
                if (singleTonObj == null) {
                    singleTonObj = new SingleTon();
                }
            }
        }
        return singleTonObj;
    }

    private Object readResolve() throws ObjectStreamException {
        System.out.println("read resolve called");
        return singleTonObj;
    }

}

class SingleTonRnD {

    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(SingleTonRnD::useSingleTon);
        executorService.submit(SingleTonRnD::useSingleTon);
        executorService.shutdown();
        SingleTon s1 = SingleTon.getInstance();
        SingleTon s2 = SingleTon.getInstance();
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("/file/s1.ser"));
        oos.writeObject(s1);
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("/file/s1.ser"));
        SingleTon s3 = (SingleTon) ois.readObject();
        print("s1", s1);
        print("s2", s2);
        print("s3", s3);
    }

    static void useSingleTon() {
        SingleTon s3 = SingleTon.getInstance();
        // print("s3", s3);
    }

    private static void print(String name, SingleTon object) {
        System.out.println(String.format("object is %s, hascode is: %d", name, object.hashCode()));
    }

}