package com.sbs.algorithm;

import java.util.HashSet;
import java.util.Set;

public class FirstRepeatingCharacter {

    public static void main(String[] args) {

        char c = getFirstRepeatedCharater("ITC HYNFOTECH".toCharArray());
        System.out.println("First repeated  c..." + c);
    }

    public static char getFirstRepeatedCharater(char[] charArray) {

        Set<Character> characterSet = new HashSet<>();

        for (char c : charArray) {
            if (characterSet.contains(c)) {
                return c;
            } else {
                characterSet.add(c);
            }
        }
        return '\0';
    }

}
