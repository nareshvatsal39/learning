package com.sbs.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerilizationTest {

    public static void main(String args[]) {
        SerilizationTest serilizationTest = new SerilizationTest();
        serilizationTest.writeOBJ();
        serilizationTest.readOBJ();

    }

    public void writeOBJ() {

        Address address = new Address();
        address.setStreet("wall street");
        address.setCountry("united states");

        try {

            FileOutputStream fout = new FileOutputStream("c:\\test\\address.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(address);
            oos.close();
            System.out.println("Done");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void readOBJ() {

        Address address;

        try {

            FileInputStream fin = new FileInputStream("c:\\test\\address.ser");
            ObjectInputStream ois = new ObjectInputStream(fin);
            address = (Address) ois.readObject();
            ois.close();

            System.out.println(address);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
